/**
 * ***************************************************
 * Autore: Rossi Stefano Nome: SuppaMarioApplet
 * ***************************************************
 */
package suppamario;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author BBC
 */
public class SuppaMarioApplet extends Applet implements Runnable, KeyListener {

    private Image imgOffScreen, imgMario, imgBg, imgKTwalking, imgKThidden, imgKtCurrent[];
    static Image imgBlock, imgWawes, imgCoinHeavenCloud, imgGround;
    private Graphics graphOffScreen;
    private Mario mario;
    private Thread thAgg = new Thread(this);
    private static URL dataPath;
    private static Background bg1, bg2;
    private KoopaTroopa[] kt;
    private Animation marioRunning, marioRunningb, marioDie;
    private Tile[][] tilemap = new Tile[2000][15];
    //audio
    private AudioClip midiLvl1, midiDead, acJump;
    private boolean alreadyDied;

    @Override
    public void init() {
        this.setSize(800, 480); //grande piu comunce degli schermi android
        this.setBackground(Color.BLACK); //imposto lo sfondo del lapplet nera

        //imposto il focus alla finestra cosi non ci devo cliccare
        this.setFocusable(true);
        this.requestFocus();

        //doppio buffering
        imgOffScreen = createImage(this.getWidth(), this.getHeight());
        graphOffScreen = imgOffScreen.getGraphics();

        //inizializzo varie variabili
        alreadyDied = false;

        //istanzio gli oggetti necessari
        bg1 = new Background(2160, 480);//le immagini sono lunghe 216,cosi
        bg2 = new Background(2160, 480);//evito errori di sovrapposizione
        //poi lo scenerei e composto da 32x32
        //array tilde map 67x25 per immagine
        mario = new Mario(300, 300);

        //imposto le immagini
        dataPath = getCodeBase();
        imgMario = getImage(dataPath, "img/mario/marioStop.gif");
        imgBg = getImage(dataPath, "img/background/background.png");
        imgBlock = getImage(dataPath, "img/scenery/block.gif");
        imgWawes = getImage(dataPath, "img/scenery/wawes.gif");
        imgCoinHeavenCloud = getImage(dataPath, "img/scenery/coinheavencloud.gif");
        imgGround = getImage(dataPath, "img/scenery/ground.gif");

        //midi generali
            midiDead = getAudioClip(getCodeBase(), "midi/die.wav");
        acJump = getAudioClip(getCodeBase(), "midi/jump.wav");

        //inizializzo la tilemap in base al file della mappa (carico la mappa)
        try {
            loadMap("build/classes/mappe/map1.txt");
            
            midiLvl1 = getAudioClip(getCodeBase(), "midi/lvl1.mid");
            midiLvl1.loop();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.out.println("IOexception loading map 1");
        }

        //passo le immagini per mario in corsa avanti e indietro
        marioRunning = new Animation();
        marioRunning.addFrame(getImage(dataPath, "img/mario/marioRun1.gif"), 50);
        marioRunning.addFrame(getImage(dataPath, "img/mario/marioRun2.gif"), 50);
        marioRunning.addFrame(getImage(dataPath, "img/mario/marioRun3.gif"), 50);
        marioRunning.addFrame(getImage(dataPath, "img/mario/marioRun2.gif"), 50);

        marioRunningb = new Animation();
        marioRunningb.addFrame(getImage(dataPath, "img/mario/marioRun1b.gif"), 50);
        marioRunningb.addFrame(getImage(dataPath, "img/mario/marioRun2b.gif"), 50);
        marioRunningb.addFrame(getImage(dataPath, "img/mario/marioRun3b.gif"), 50);
        marioRunningb.addFrame(getImage(dataPath, "img/mario/marioRun2b.gif"), 50);

        //passo le immagini per la morte di mario
        marioDie = new Animation();
        marioDie.addFrame(getImage(dataPath, "img/mario/die1.gif"), 0);
        marioDie.addFrame(getImage(dataPath, "img/mario/die2.gif"), 100);
        marioDie.addFrame(getImage(dataPath, "img/mario/die3.gif"), 200);
        marioDie.addFrame(getImage(dataPath, "img/mario/die4.gif"), 200);
        marioDie.addFrame(getImage(dataPath, "img/mario/die5.gif"), 200);
        marioDie.addFrame(getImage(dataPath, "img/mario/die6.gif"), 200);
        marioDie.addFrame(getImage(dataPath, "img/mario/die7.gif"), 200);

        //attivo il keylistener in questa applet
        this.addKeyListener(this);

        thAgg.start();
    }

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void destroy() {
    }

    //METODI APPLET DI DISEGNO
    @Override
    public void update(Graphics g) {
        //ridisegno la sprite se necessario di mario
        if (mario.isDead()) {
            imgMario = marioDie.getImage();
            marioDie.update(17);
        } else if (mario.isDucked()) {
            imgMario = getImage(dataPath, "img/mario/marioDucked.gif");
        } else if (mario.getSpeedY() != 0 || mario.isJumped()) {
            if (mario.isTurnedRight()) {
                imgMario = getImage(dataPath, "img/mario/marioJump.gif");
            } else {
                imgMario = getImage(dataPath, "img/mario/marioJumpb.gif");
            }
        } else if (mario.isMoving()) {
            marioRunning.update(17);
            marioRunningb.update(17);
            if (mario.isTurnedRight()) {
                imgMario = marioRunning.getImage();
            } else {
                imgMario = marioRunningb.getImage();
            }
        } else {
            if (mario.isTurnedRight()) {
                imgMario = getImage(dataPath, "img/mario/marioStop.gif");
            } else {
                imgMario = getImage(dataPath, "img/mario/marioStopb.gif");
            }
        }
        //ridisegno la sprite se necessario dei koopatroopa
        for(int i = 0; i < kt.length; i++){
            if(kt[i].getCurrentHealth() == kt[i].getMaxHealth()){
                imgKtCurrent[i] = getImage(dataPath, "img/enemies/koopaTroopa/greenWalking.gif");
            }else{
                imgKtCurrent[i] = getImage(dataPath, "img/enemies/koopaTroopa/greenHidden.gif");
            }
        }
        
        paint(g);
    }

    @Override
    public void paint(Graphics g) {
        //resetto lo scehrmo di buff
        graphOffScreen.clearRect(0, 0, imgOffScreen.getWidth(this), imgOffScreen.getWidth(this));

        //disegno i backdrounds
        graphOffScreen.drawImage(imgBg, bg1.getBgX(), 0, this);
        graphOffScreen.drawImage(imgBg, bg2.getBgX(), 0, this);

        //stampo la tilemap
        for (int i = 0; i < tilemap.length; i++) {
            for (int j = 0; j < tilemap[0].length; j++) {
                if (tilemap[i][j] != null) {
                    graphOffScreen.drawImage(tilemap[i][j].getTileImage(), tilemap[i][j].getTileX(), tilemap[i][j].getTileY(), 32, 32, this);
                }
            }
        }

        //disegno mario in base a quanto e grande
        graphOffScreen.drawImage(imgMario, mario.getCenterX() - 13, mario.getCenterY() - 16, this);

        //quadrato collisioni sprites//
//        graphOffScreen.drawRect((int) Mario.rUpCollision.getLocation().getX(), (int) Mario.rUpCollision.getLocation().getY(), (int) Mario.rUpCollision.getWidth(), (int) Mario.rUpCollision.getHeight());
//        graphOffScreen.drawRect((int) Mario.rDownCollision.getLocation().getX(), (int) Mario.rDownCollision.getLocation().getY(), (int) Mario.rDownCollision.getWidth(), (int) Mario.rDownCollision.getHeight());
//        graphOffScreen.drawRect((int) Mario.rLeftCollision.getLocation().getX(), (int) Mario.rLeftCollision.getLocation().getY(), (int) Mario.rLeftCollision.getWidth(), (int) Mario.rLeftCollision.getHeight());
//        graphOffScreen.drawRect((int) Mario.rRightCollision.getLocation().getX(), (int) Mario.rRightCollision.getLocation().getY(), (int) Mario.rRightCollision.getWidth(), (int) Mario.rRightCollision.getHeight());
//        graphOffScreen.drawRect((int) Mario.rSurroundingMario.getLocation().getX(), (int) Mario.rSurroundingMario.getLocation().getY(), (int) Mario.rSurroundingMario.getWidth(), (int) Mario.rSurroundingMario.getHeight());

        //disegno i kt
        for (int i = 0; i < kt.length; i++) {
            if (kt[i].isVisible()) {
                //graphOffScreen.drawRect((int) kt[i].getCollision().getLocation().getX(), (int) kt[i].getCollision().getLocation().getY(), (int) kt[i].getCollision().getWidth(),(int) kt[i].getCollision().getHeight());
                graphOffScreen.drawImage(imgKtCurrent[i], kt[i].getCenterX() - 10, kt[i].getCenterY() - ((kt[i].getCurrentHealth() != 2)?0:15), 20, (kt[i].getCurrentHealth() != 2)?15:30, this);
            }
        }
        /////////////////////////////

        g.drawImage(imgOffScreen, 0, 0, this);
    }

    private void loadMap(String mapPath) throws IOException {
        //salvo le linee in un arraylist
        ArrayList lines = new ArrayList();
        int w = 0, h = 0;

        //in questa variabile carico il file
        BufferedReader reader = new BufferedReader(new FileReader(mapPath));

        //ripeto fino a che tutte le linee sono state lette e le salvo nel arraylist
        while (true) {
            String line = reader.readLine();
            // no more lines to read
            if (line == null) {
                reader.close();
                break;
            }

            //se la linea non comincia per ! la conteggio
            if (!line.startsWith("!")) {
                lines.add(line);
                //come l unghezza massimo imposto quella della riga piu lunga
                w = Math.max(w, line.length());
            }
        }
        h = lines.size();

        //ora posso inserire riga per riga i tile nella tilemap
        for (int j = 0; j < 15 && j < h; j++) {
            String line = (String) lines.get(j);
            for (int i = 0; i < w; i++) {
                if (i < line.length()) {
                    char ch = line.charAt(i);
                    tilemap[i][j] = new Tile(i, j, ch);
                }
            }
        }
        
        //istanzio i nemici in bse alla mappa
        int ktLevel = 4;
        
        kt = new KoopaTroopa[ktLevel];
        kt[0] = new KoopaTroopa(600, 400);
        kt[1] = new KoopaTroopa(800, 400);
        kt[2] = new KoopaTroopa(1100, 400);
        kt[3] = new KoopaTroopa(1400, 400);
        
        imgKtCurrent = new Image[ktLevel];
    }

    //GETTER
    static Background getBg1() {
        return bg1;
    }

    static Background getBg2() {
        return bg2;
    }

    public static URL getDataPath() {
        return dataPath;
    }
    //METODI DEL KEYLISTENER

    @Override
    public void keyPressed(KeyEvent e) {
        //uso uno switch per il tasto premuto
        switch (e.getKeyCode()) {

            case KeyEvent.VK_UP:
                if ((mario.getSpeedY() == 0)) {
                    mario.jump();
                    acJump.stop();
                    acJump.play();
                }
                break;

            case KeyEvent.VK_DOWN:
                mario.setDuck(true);
                break;

            case KeyEvent.VK_LEFT:
                mario.moveLeft();
                break;

            case KeyEvent.VK_RIGHT:
                mario.moveRight();
                break;

            case KeyEvent.VK_SPACE:
                if ((mario.getSpeedY() == 0)) {
                    mario.jump();
                    acJump.stop();
                    acJump.play();
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {

            case KeyEvent.VK_UP:
                if (!mario.isDead()) {
                    mario.stopJumping();
                }
                break;

            case KeyEvent.VK_DOWN:
                mario.setDuck(false);
                break;

            case KeyEvent.VK_LEFT:
                mario.stop();
                break;

            case KeyEvent.VK_RIGHT:
                mario.stop();
                break;

            case KeyEvent.VK_SPACE:
                mario.stopJumping();
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    private void checkMarioEnemyCollision() {
        for (int i = 0; i < kt.length; i++) {
            if (kt[i].isVisible()) {
                if (kt[i].isVisible() && kt[i].checkCollision(Mario.rSurroundingMario)) {
                    if (kt[i].checkCollision(Mario.rDownCollision)) {
                        mario.enemyHit();
                        acJump.stop();
                        acJump.play();
                        kt[i].hitten();
                    } else if (kt[i].checkCollision(Mario.rLeftCollision) || kt[i].checkCollision(Mario.rRightCollision) || kt[i].checkCollision(Mario.rUpCollision)) {
                        if (!mario.isDead()) {
                            mario.die();
                            kt[i].setVisible(false);
                        }
                    }
                }
            }
        }
    }

    private synchronized void checkMarioTileCollision() {
        //per ogni tile controllo la collisione per fermare il salto
        for (int i = 0; i < tilemap.length; i++) {
            for (int j = 0; j < tilemap[0].length; j++) {

                //prima controllare ogni singolo rectangle controllo che quello che racchiude tutti collisioni col tipe attuale
                if (tilemap[i][j] != null && tilemap[i][j].checkCollision(Mario.rSurroundingMario)) {
                    //check dalla collisionesuperiore e inferiore
                    //se questo tile non e vuotoe e non e un blocco che uccide(es: onde)

                    if (tilemap[i][j].isSolid() && !tilemap[i][j].isKiller()) {
                        //System.out.println(tilemap[i][j].getType());
                        if (tilemap[i][j].checkCollision(Mario.rUpCollision)) {
                            mario.upCollisionDetected();
                        } else if (tilemap[i][j].checkCollision(Mario.rDownCollision)) {
                            if (tilemap[i][j].isKiller()) {
                                if (!mario.isDead()) {
                                    mario.die();
                                }
                            } else if (!mario.isDead()) {
                                mario.downCollisionDetected();
                                Tile.checkpointX = tilemap[0][0].getTileX();
                            }
                        }
                    }

                    //collisioni sx e dx
                    if (tilemap[i][j].isSolid()) {
                        //se non e null e non e un carattere vuoto allora controllo la collisione (tab e barra spaziatrice spazio)
                        if (tilemap[i][j].checkCollision(Mario.rLeftCollision)) {
                            //se e acqua muore
                            if (tilemap[i][j].isKiller()) {
                                if (!mario.isDead()) {
                                    mario.die();
                                }
                            } else {
                                mario.leftCollisionDetected();
                            }
                        }

                        if (tilemap[i][j].checkCollision(Mario.rRightCollision)) {
                            //se e acqua muore
                            if (tilemap[i][j].isKiller()) {
                                if (!mario.isDead()) {
                                    mario.die();
                                }
                            } else {
                                mario.rightCollisionDetected();
                            }
                        }
                    }
                }
            }
        }
    }

    //ricarico la posizione della tilmap prima di morire
    private void loadCheckPointTile() {
        for (int i = 0; i < tilemap.length; i++) {
            for (int j = 0; j < tilemap[0].length; j++) {
                if (tilemap[i][j] != null) {
                    tilemap[i][j].setTileX(tilemap[i][j].getTileX() + (Math.abs(Tile.absX) - Math.abs(Tile.checkpointX)));
                }
            }
        }
    }

    //RUNNABLE
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep((mario.isDead()) ? 50 : 17);
                //aggiorno la posizione degli elementi e ridisegno la applet
                mario.update();
                bg1.update();
                bg2.update();

                //questi invece li aggiorno solo se mario non e morto
                //collisioni comprese
                if (!mario.isDead()) {
                    for (int i = 0; i < kt.length; i++) {
                        kt[i].update();
                    }

                    //agiornamento tilemap
                    for (int i = 0; i < tilemap.length; i++) {
                        for (int j = 0; j < tilemap[0].length; j++) {
                            if (tilemap[i][j] != null) {
                                tilemap[i][j].update();
                            }
                        }
                    }

                    //eseguo un save di absX della tilemap
                    Tile.absX = tilemap[0][0].getTileX();

                    //check delle colllisioni tra mario e tilemap
                    checkMarioTileCollision();

                    //check delle colllisioni tra mario e i nemici
                    checkMarioEnemyCollision();
                }

                //se dopo queste collisioni mario muore, per una sola volta
                if (mario.isDead() && !alreadyDied) {
                    alreadyDied = true;
                    midiLvl1.stop();
                    midiDead.play();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }

                //controllo la posizione di mario e la resetto, succede se e morto
                if (mario.isDead() && mario.getCenterY() > 1000) {
                    loadCheckPointTile();
                    mario.revive();
                    alreadyDied = false;
                    marioDie.reset();

                    midiLvl1.loop();
                }

                repaint();
                //System.out.println("agg");
            } catch (InterruptedException e) {
            }
        }
    }
}