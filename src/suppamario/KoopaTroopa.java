/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

/**
 *
 * @author BBC
 */
public class KoopaTroopa extends Enemy implements Runnable{
    Thread th;
    
    public KoopaTroopa(int centerX, int centerY){
        super(2);
        this.setCenterX(centerX);
        this.setCenterY(centerY);
    }
    
    @Override
    void hitten() {
       if(--currentHealth == 0){
           die();
       }
       speedX = 0;
       
       collision.setBounds(centerX - 12, centerY, 23, 16);
       
       th = new Thread(this);
       th.start();
    }

    @Override
    public void run() {
        try {
            //aspetto un lasso di tempo e poi incremento la vita del kt            
            Thread.sleep(2000);
            
            currentHealth++;
            
            if (visible) {
                //se mario va a destra e il bg a sx muovo a dx
                if (bg.getSpeedX() == bg.SPEEDSTOP - bg.SPEEDMOV) {
                    speedX = bg.SPEEDMOV * 2;
                    //se mario va a sx e il bg a dx muovo a sx
                } else if (bg.getSpeedX() == bg.SPEEDSTOP + bg.SPEEDMOV) {
                    speedX = -bg.SPEEDMOV * 2;
                    //se il bg non si muove
                } else {
                    speedX = 0; 
                }
            }
            
            System.out.println("speedx " + speedX);
        } catch (InterruptedException ex) {
            //interrupted
        }        
    }
}
