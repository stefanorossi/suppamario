/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

import java.awt.Image;
import java.awt.Rectangle;

/**
 *
 * @author BBC
 */
public class Tile {
    // ogni tile sara 32x32 qundi per trovare la posizione
    // bastera fare l indice(in cui e posiz nella tilemap)
    //e per la posizione 32 * X o Y

    private int tileX, tileY, speedX, type;
    //type è il tipo (terra, acqua, nuvola, ecc)
    //speed e la velocita del pezzo
    Image tileImage;
    private Rectangle rCollision;
    Background bg = SuppaMarioApplet.getBg1();
    private boolean killer = false; // di default false, se questo tile uccide diventa true
    private boolean solid = false; //se il blocco e solido
    
    //variabili statiche per il checkpoint
    static int absX = 0; //variabile variata sempre tramite update
    static int checkpointX = 0; //variabile per il checkpoint
    
    static void setCheckPointX(int cpX){
        checkpointX = cpX;
    }
    static int getCheckPointX(){
        return checkpointX;
    }
    
    public Tile(int X, int Y, char typeInt) {
        this.tileX = X * 32;
        this.tileY = Y * 32;
        this.type = typeInt;
        this.rCollision = new Rectangle(tileX, tileY, 32, 32);
        //System.out.println(this.tileX + " " + this.tileY);

        this.tileImage = chooseImage(typeInt);
    }

    //spostamento del pezzo
    public void update() {

        //se mario va a destra e il bg a sx muovo a dx
        if (bg.getSpeedX() == bg.SPEEDSTOP - bg.SPEEDMOV) {
            speedX = bg.SPEEDMOV * 2;
            //se mario va a sx e il bg a dx muovo a sx
        } else if (bg.getSpeedX() == bg.SPEEDSTOP + bg.SPEEDMOV) {
            speedX = -bg.SPEEDMOV * 2;
            //se il bg non si muove
        } else {
            speedX = 0;
        }
        // Se il pezzo e una wawes quindi si muove sempre un po (type  == 0)
        if (this.type == 0) {
            speedX -= 1;
        }

        tileX += speedX;

        this.rCollision.setBounds(this.tileX, this.tileY, 32, 32);

        //se non e un tile vuoto allora controllo se avviene una collisione
        //if(this.type != 9) checkCollision(Mario.rCollision);        
    }

    //check delle collisioni tra il rettangolo passato e il rettangolo del tile
    //se avessi a che fare con piu rectangles potrei creare un rettangolo piu
    //grande, che racchiude tutti gli altri e prima verificare se il tile e dentro
    //a quello, se non lo e non serve verificare anche tutti gli altri
    //cosi facnedo si risparmiano molte operazioni
    public boolean checkCollision(Rectangle r) {
        if (r.intersects(rCollision)) {
            return true;
        } else {
            return false;
        }
    }

    //imposto l immagine altile in base al tipo
    private Image chooseImage(char typeInt) {

        switch (typeInt) {
            case 'w':
                this.killer = true;
                this.solid = true;
                return SuppaMarioApplet.imgWawes;
            case 'g':
                this.solid = true;
                return SuppaMarioApplet.imgGround;
            case 'c':
                this.solid = false;
                return SuppaMarioApplet.imgCoinHeavenCloud;
            case 'b':
                this.solid = true;
                return SuppaMarioApplet.imgBlock;
            default:
                return null;
        }
    }

    //GETTER
    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getType() {
        return type;
    }

    public Image getTileImage() {
        return tileImage;
    }

    public Rectangle getrCollision() {
        return rCollision;
    }

    public Background getBg() {
        return bg;
    }

    public boolean isKiller() {
        return killer;
    }
    
    public boolean isSolid() {
        return solid;
    }

    public static int getAbsX() {
        return absX;
    }

    public static int getCheckpointX() {
        return checkpointX;
    }
    
    //setter
    public void setTileX(int tileX) {
        this.tileX = tileX;
    }

    public void setTileY(int tileY) {
        this.tileY = tileY;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setTileImage(Image tileImage) {
        this.tileImage = tileImage;
    }

    public void setrCollision(Rectangle rCollision) {
        this.rCollision = rCollision;
    }

    public void setBg(Background bg) {
        this.bg = bg;
    }

    public static void setAbsX(int absX) {
        Tile.absX = absX;
    }

    public static void setCheckpointX(int checkpointX) {
        Tile.checkpointX = checkpointX;
    }
}
