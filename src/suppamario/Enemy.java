/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

import java.awt.Rectangle;

/**
 *
 * @author BBC
 */
public class Enemy{

    int maxHealth, currentHealth, power, speedX, centerX, centerY;
    boolean visible = true, died = false;
    Background bg = SuppaMarioApplet.getBg1();
    Rectangle collision = new Rectangle(0, 0, 0, 0);

    public Enemy(int maxHealth){
        this.maxHealth = maxHealth;
        this.currentHealth = this.maxHealth;
    }
    
    //METODI
    public void update() {
        if (visible && currentHealth != 1) {
            //se mario va a destra e il bg a sx muovo a dx
            if (bg.getSpeedX() == bg.SPEEDSTOP - bg.SPEEDMOV) {
                speedX = bg.SPEEDMOV * 2;
                //se mario va a sx e il bg a dx muovo a sx
            } else if (bg.getSpeedX() == bg.SPEEDSTOP + bg.SPEEDMOV) {
                speedX = -bg.SPEEDMOV * 2;
                //se il bg non si muove
            } else {
                speedX = 0;
            }

            centerX += speedX - 1; //spostamento in base al tilde piu il suo spostamento di 1

            collision.setBounds(centerX - 12, centerY - 16, 23, 32);
        }
    }

    boolean checkCollision(Rectangle inRect) {
        return collision.intersects(inRect);
    }
    
    void hitten() {
       if(--this.currentHealth == 0){
           die();
       }
    }

    public void die() {
        this.visible=false;
        this.died = true;
    }

    public void attack() {
    }

    //GETTER
    public int getMaxHealth() {
        return maxHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public boolean isVisible() {
        return visible;
    }

    public int getPower() {
        return power;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public Background getBg() {
        return bg;
    }

    public Rectangle getCollision() {
        return collision;
    }

    //SETTER
    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public void setBg(Background bg) {
        this.bg = bg;
    }

    public void setCollision(Rectangle collision) {
        this.collision = collision;
    }

    public void setVisible(boolean visibility) {
        this.visible = visibility;
    }

    public boolean isDied() {
        return died;
    }

    public void setDied(boolean died) {
        this.died = died;
    }
}
