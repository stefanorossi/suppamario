/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package suppamario;

/**
 *
 * @author BBC
 */
public class Background {
    //con queste variabili conteggio i bgs istanziati e la somma della 
    //loro lunghezza in modo da permette piu background consecutivi
    // MASSIMO 2 ISTANZE DI BG ALTRIMENTI NON FUNZIONA  
    private static int cntBgs = 0, totWbgs = 0;  
    
    final static int SPEEDMOV = 2, SPEEDSTOP = 1;
    private static int speedX = SPEEDSTOP;
    
    private int bgX; //coord x che viene incrementata
    private int bgW;
    private int bgH;

    //costruttore 
    public Background(int bgW, int bgH) {
        this.bgW = bgW;
        this.bgH = bgH;
        this.bgX = Background.totWbgs;
        
        Background.cntBgs++;
        totWbgs += bgW;
    }
    
    //METODI
    public synchronized void update() {
        //scrollo in x in base alla velocita x
        this.bgX -= speedX;

        //se sono fuori dai limiti resetto la sua posizione a dx
        if (speedX >= SPEEDSTOP + SPEEDMOV && this.bgX <= -this.bgW) {
            this.bgX = bgW;
        //se la velocita e negativa il controllo e diverso
        //per assicurare il bg anche andando a sx
        }else if(speedX <= SPEEDSTOP - SPEEDMOV && this.bgX >= this.bgW){
            this.bgX = -this.bgW;
        }
    }
    
    public synchronized void moveLeft(){
        speedX = SPEEDSTOP - SPEEDMOV; 
    }
    
    public synchronized void moveRight(){
        speedX = SPEEDSTOP + SPEEDMOV;
    }
    
    public synchronized void moveStop(){
        speedX = SPEEDSTOP;
    }
    
    //GETTER
    public int getBgX() {
        return bgX;
    }

    public int getBgW() {
        return bgW;
    }

    public int getBgH() {
        return bgH;
    }

    public int getSpeedX() {
        return speedX;
    }
    
}
