# README #

#Requirement:
JDK 6 + NetBeans

#Version 0.1
-Idle cycle background. Speeding according with mario movement speed

-Mario 
	
	-collide with solid blocks
	
	- can jump and crouch

	- dies when hit by lateral collision with koopa, by deadly blocks (water) or dropping out of the map

	- has dead, jump and crouch animation

-Background music and die animation music

-The map is loaded from an esternal file, you can build with:

	- 2 solid block
	
	- 1 pass-throug block
	
	- 1 deadly block
	
-Implemented 2 walking koopa enemies, they die and disappear on upper collision with mario



#Installation and run

Clone the repo:
git clone https://stefanorossi@bitbucket.org/stefanorossi/supermario.git

This is a NetBeans Project. Using NetBeans isnt mandatory but recommended for the buil in applet runner.
You can run the SuppaMarioApplet.java class with netbeans.



#Description
This is a unfinished early version clone of supermario.

![Scheme](images/suppamario.png)

You can create you own map modifiyng the map file:
\suppaMariov6\build\classes\mappe\map1.txt

![Scheme](images/suppamario2.png)


#License 


All my work is released under [DBAD](https://www.dbad-license.org/) license.